package br.com.cartoes.pagamento.controllers;

import br.com.cartoes.pagamento.DTOs.ConsultarPagamentoDTO;
import br.com.cartoes.pagamento.DTOs.CriarPagamentoDTO;
import br.com.cartoes.pagamento.DTOs.ListaPagamentoDTO;
import br.com.cartoes.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public ConsultarPagamentoDTO novoPagamento(@RequestBody @Valid CriarPagamentoDTO criarPagamentoDTO){
        try {
            return pagamentoService.novoPagamento(criarPagamentoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<ListaPagamentoDTO> consultarPagamentos(@PathVariable(name = "cartaoId") int cartaoId){
        try {
            return pagamentoService.consultarPagamentos(cartaoId);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
