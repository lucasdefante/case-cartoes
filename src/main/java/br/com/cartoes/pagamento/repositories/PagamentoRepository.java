package br.com.cartoes.pagamento.repositories;

import br.com.cartoes.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findByCartaoId(int cartaoId);
    void deleteByCartaoId(int cartaoId);
}
