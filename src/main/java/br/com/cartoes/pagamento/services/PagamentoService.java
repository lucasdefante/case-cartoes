package br.com.cartoes.pagamento.services;

import br.com.cartoes.cartao.services.CartaoService;
import br.com.cartoes.pagamento.DTOs.ConsultarPagamentoDTO;
import br.com.cartoes.pagamento.DTOs.CriarPagamentoDTO;
import br.com.cartoes.pagamento.DTOs.ListaPagamentoDTO;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.pagamento.models.Pagamento;
import br.com.cartoes.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public ConsultarPagamentoDTO novoPagamento(CriarPagamentoDTO criarPagamentoDTO) {
        Cartao cartao = cartaoService.consultarCartaoPorId(criarPagamentoDTO.getCartaoId());
        if(!cartao.isAtivo()){
            throw new RuntimeException("Cartão inativo. Não é possível utilizá-lo para compras.");
        }
        Pagamento pagamento = criarPagamentoDTO.converterParaPagamento(cartao);
        Pagamento pagamentoDB = pagamentoRepository.save(pagamento);
        cartao.adicionarPagamento(pagamentoDB);
        ConsultarPagamentoDTO consultarPagamentoDTO = new ConsultarPagamentoDTO(pagamentoDB.getId(), pagamentoDB.getCartao().getId(), pagamentoDB.getDescricao(), pagamentoDB.getValor());
        return consultarPagamentoDTO;
    }

    public List<ListaPagamentoDTO> consultarPagamentos(int cartaoId) {
        List<Pagamento> listaPagamentos = (List) pagamentoRepository.findByCartaoId(cartaoId);
        if(listaPagamentos.size() == 0) {
            throw new RuntimeException("Não há pagamentos a serem faturados para o cartão consultado");
        }
        List<ListaPagamentoDTO> listaPagamentosDTO = new ArrayList<>();
        for (Pagamento pagamento:
                listaPagamentos) {
            ListaPagamentoDTO listaPagamentoDTO = new ListaPagamentoDTO(pagamento.getId(), pagamento.getCartao().getId(), pagamento.getDescricao(), pagamento.getValor());
            listaPagamentosDTO.add(listaPagamentoDTO);
        }
        return listaPagamentosDTO;
    }

    public void deletarPagamentos(int cartaoId) {
        pagamentoRepository.deleteByCartaoId(cartaoId);
    }
}
