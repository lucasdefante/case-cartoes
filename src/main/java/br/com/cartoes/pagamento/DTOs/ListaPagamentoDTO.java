package br.com.cartoes.pagamento.DTOs;

public class ListaPagamentoDTO {

    private int id;
    private int cartaoId;
    private String descricao;
    private double valor;

    public ListaPagamentoDTO() {
    }

    public ListaPagamentoDTO(int id, int cartaoId, String descricao, double valor) {
        this.id = id;
        this.cartaoId = cartaoId;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(int cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
