package br.com.cartoes.pagamento.DTOs;

import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.pagamento.models.Pagamento;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CriarPagamentoDTO {

    @NotNull(message = "Id do cartão não deve ser nula")
    private int cartaoId;

    @NotNull(message = "Descrição não deve ser nula")
    @NotBlank(message = "Descrição não deve ser em branco")
    private String descricao;

    @NotNull(message = "Valor não deve ser nulo")
    private double valor;

    public CriarPagamentoDTO() {
    }

    public int getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(int cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Pagamento converterParaPagamento(Cartao cartao){
        Pagamento pagamento = new Pagamento();
        pagamento.setCartao(cartao);
        pagamento.setDescricao(descricao);
        pagamento.setValor((new BigDecimal(valor).setScale(2, RoundingMode.DOWN)).doubleValue());
        return pagamento;
    }
}
