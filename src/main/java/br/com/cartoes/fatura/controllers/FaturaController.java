package br.com.cartoes.fatura.controllers;

import br.com.cartoes.fatura.DTOs.ExpirarCartaoDTO;
import br.com.cartoes.pagamento.DTOs.ListaPagamentoDTO;
import br.com.cartoes.fatura.DTOs.PagarFaturaDTO;
import br.com.cartoes.fatura.services.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @GetMapping("/{clienteId}/{cartaoId}")
    public List<ListaPagamentoDTO> exibirFatura(@PathVariable(name = "clienteId") int clienteId,
                                                @PathVariable(name = "cartaoId") int cartaoId) {
        try {
            return faturaService.exibirFatura(clienteId, cartaoId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{clienteId}/{cartaoId}/pagar")
    public PagarFaturaDTO pagarFatura(@PathVariable(name = "clienteId") int clienteId,
                                      @PathVariable(name = "cartaoId") int cartaoId) {
        try {
            return faturaService.pagarFatura(clienteId, cartaoId);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{clienteId}/{cartaoId}/expirar")
    public ExpirarCartaoDTO expirarCartao(@PathVariable(name = "clienteId") int clienteId,
                                          @PathVariable(name = "cartaoId") int cartaoId) {
        try {
            return faturaService.expirarCartao(clienteId, cartaoId);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
