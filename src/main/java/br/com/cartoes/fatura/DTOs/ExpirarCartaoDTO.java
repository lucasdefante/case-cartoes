package br.com.cartoes.fatura.DTOs;

public class ExpirarCartaoDTO {

    private String status;

    public ExpirarCartaoDTO() {
    }

    public ExpirarCartaoDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
