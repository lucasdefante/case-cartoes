package br.com.cartoes.fatura.services;

import br.com.cartoes.cartao.services.CartaoService;
import br.com.cartoes.fatura.DTOs.ExpirarCartaoDTO;
import br.com.cartoes.pagamento.DTOs.ListaPagamentoDTO;
import br.com.cartoes.fatura.DTOs.PagarFaturaDTO;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.fatura.models.Fatura;
import br.com.cartoes.fatura.repositories.FaturaRepository;
import br.com.cartoes.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoService cartaoService;

    public List<ListaPagamentoDTO> exibirFatura(int clienteId, int cartaoId) {
        Cartao cartao = cartaoService.consultarCartaoPorId(cartaoId);
        if(cartao.getCliente().getId() != clienteId) {
            throw new RuntimeException("Cartão consultado não pertence ao cliente especificado");
        }
        return pagamentoService.consultarPagamentos(cartaoId);
    }

    @Transactional
    public PagarFaturaDTO pagarFatura(int clienteId, int cartaoId) {
        List<ListaPagamentoDTO> listaPagamentoDTOS = exibirFatura(clienteId, cartaoId);
        double valorPago = 0;
        LocalDate pagoEm = LocalDate.now();
        for (ListaPagamentoDTO pagamento:
             listaPagamentoDTOS) {
            valorPago += pagamento.getValor();
        }
        pagamentoService.deletarPagamentos(cartaoId);
        Fatura fatura = new Fatura();
        fatura.setCartao(cartaoService.consultarCartaoPorId(cartaoId));
        fatura.setPagoEm(pagoEm);
        fatura.setValorPago((new BigDecimal(valorPago).setScale(2, RoundingMode.DOWN)).doubleValue());

        Fatura faturaDB = faturaRepository.save(fatura);
        return new PagarFaturaDTO(faturaDB.getId(), faturaDB.getValorPago(), faturaDB.getPagoEm());
    }

    public ExpirarCartaoDTO expirarCartao(int clienteId, int cartaoId) {
        Cartao cartao = cartaoService.consultarCartaoPorId(cartaoId);
        if(cartao.getCliente().getId() != clienteId) {
            throw new RuntimeException("Cartão consultado não pertence ao cliente especificado");
        }
        cartaoService.ativarOuDesativar(cartao.getNumero(), false);
        return new ExpirarCartaoDTO("ok");
    }
}
