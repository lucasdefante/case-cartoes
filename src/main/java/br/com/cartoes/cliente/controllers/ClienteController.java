package br.com.cartoes.cliente.controllers;

import br.com.cartoes.cliente.models.Cliente;
import br.com.cartoes.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
        return clienteService.criarCliente(cliente);
    }

    @GetMapping("/{id}")
    public Cliente consultarClientePorId(@PathVariable(name = "id") int id){
            return clienteService.consultarClientePorId(id);
    }
}
