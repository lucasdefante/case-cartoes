package br.com.cartoes.cliente.services;

import br.com.cartoes.cliente.exceptions.ClientNotFoundException;
import br.com.cartoes.cliente.models.Cliente;
import br.com.cartoes.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            return clienteOptional.get();
        } else {
            throw new ClientNotFoundException();
        }
    }
}
