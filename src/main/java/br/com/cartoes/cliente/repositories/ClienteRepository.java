package br.com.cartoes.cliente.repositories;

import br.com.cartoes.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
