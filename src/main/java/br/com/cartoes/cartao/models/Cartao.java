package br.com.cartoes.cartao.models;

import br.com.cartoes.cliente.models.Cliente;
import br.com.cartoes.fatura.models.Fatura;
import br.com.cartoes.pagamento.models.Pagamento;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String numero;

    @OneToOne
    private Cliente cliente;

    @OneToMany(mappedBy = "cartao")
    private List<Pagamento> pagamentos;

    @OneToMany(mappedBy = "cartao")
    private List<Fatura> faturas;

    private boolean ativo;

    public Cartao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamento> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public void adicionarPagamento(Pagamento pagamento) {
        this.pagamentos.add(pagamento);
    }

    public List<Fatura> getFaturas() {
        return faturas;
    }

    public void setFaturas(List<Fatura> faturas) {
        this.faturas = faturas;
    }
}
