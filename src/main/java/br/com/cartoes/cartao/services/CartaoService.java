package br.com.cartoes.cartao.services;

import br.com.cartoes.cartao.DTOs.CadastrarCartaoDTO;
import br.com.cartoes.cartao.DTOs.ConsultarCartaoDTO;
import br.com.cartoes.cartao.DTOs.GetCartaoDTO;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.cliente.models.Cliente;
import br.com.cartoes.cartao.repositories.CartaoRepository;
import br.com.cartoes.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;


    public ConsultarCartaoDTO criarCartao(CadastrarCartaoDTO cadastrarCartaoDTO) {
        try {

            Cliente cliente = clienteService.consultarClientePorId(cadastrarCartaoDTO.getClienteId());
            Cartao cartao = cadastrarCartaoDTO.converterParaCartao(cliente);
            Cartao cartaoDB = cartaoRepository.save(cartao);
            ConsultarCartaoDTO consultarCartaoDTO = new ConsultarCartaoDTO(cartaoDB.getId(), cartaoDB.getNumero(), cartaoDB.getCliente().getId(), cartaoDB.isAtivo());

            return consultarCartaoDTO;

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public ConsultarCartaoDTO ativarOuDesativar(String numero, boolean ativo) {

        Optional<Cartao> optionalCartaoDB = cartaoRepository.findByNumero(numero);

        if(optionalCartaoDB.isPresent()){

            Cartao cartaoDB = optionalCartaoDB.get();
            cartaoDB.setAtivo(ativo);
            Cartao cartaoSalvo = cartaoRepository.save(cartaoDB);
            ConsultarCartaoDTO consultarCartaoDTO = new ConsultarCartaoDTO(cartaoSalvo.getId(), cartaoSalvo.getNumero(), cartaoSalvo.getCliente().getId(), cartaoSalvo.isAtivo());

            return consultarCartaoDTO;

        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Cartao consultarCartaoPorNumero(String numero) {
        Optional<Cartao> optionalCartaoDB = cartaoRepository.findByNumero(numero);

        if (optionalCartaoDB.isPresent()) {
            Cartao cartaoDB = optionalCartaoDB.get();
            return cartaoDB;
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Cartao consultarCartaoPorId(int cartaoId) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(cartaoId);
        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não existe");
        }
    }

    public GetCartaoDTO getCartao(String numero){
        Cartao cartaoDB = consultarCartaoPorNumero(numero);
        GetCartaoDTO getCartaoDTO = new GetCartaoDTO(cartaoDB.getId(), cartaoDB.getNumero(), cartaoDB.getCliente().getId());
        return getCartaoDTO;
    }
}
