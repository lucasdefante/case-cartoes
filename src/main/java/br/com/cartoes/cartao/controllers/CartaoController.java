package br.com.cartoes.cartao.controllers;

import br.com.cartoes.cartao.DTOs.CadastrarCartaoDTO;
import br.com.cartoes.cartao.DTOs.ConsultarCartaoDTO;
import br.com.cartoes.cartao.DTOs.GetCartaoDTO;
import br.com.cartoes.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ConsultarCartaoDTO criarCartao(@RequestBody @Valid CadastrarCartaoDTO cadastrarCartaoDTO){
        try {
            return cartaoService.criarCartao(cadastrarCartaoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public ConsultarCartaoDTO ativarOuDesativarCartao(@RequestBody Map<String, Boolean> map, @PathVariable(name = "numero") String numero){
        try {
            if (map.containsKey("ativo")){
                return cartaoService.ativarOuDesativar(numero, map.get("ativo"));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Campo inserido inesperado");
            }
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public GetCartaoDTO consultarCartao(@PathVariable(name = "numero") String numero){
        try {
            return cartaoService.getCartao(numero);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
